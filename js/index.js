const Counter = React.createClass({
  getInitialState: function() {
    return {
      counter: 0
    };
  },

  increment: function() {
    this.setState({
      counter: this.state.counter + 1
    });
  },

  decrement: function() {
    this.setState({
      counter: this.state.counter - 1
    });
  },

  getDefaultProps: function() {
    console.log(
      "Propsy domyslne. Przydatne w momencie, kiedy tworzymy elementy z powtarzającą się zawartoscia"
    );
  },

  componentWillMount: function() {
    console.log(
      "Możemy wykonać różne operacje, kalkulacje oparte na stanie i propsach, w stanie przed renderowaniem"
    );
  },

  componentDidMount: function() {
    console.log("DOm załadowany");
  },

  componentWillReceiveProps: function() {
    console.log(
      "W momencie pojawienia się nowych propsów możemy wykonać różne operacje"
    );
  },

  shouldComponentUpdate: function() {
    console.log(
      "Można zmienić na false, jeśli nie chcemy, żeby aplikacja wywołała zmiany spowodowane przez zmianę propsów  lub stanu"
    );
    return true;
  },

  componentWillUpdate: function() {
    console.log("Podobnie jak componentWIllMount");
  },

  componentWillUnmount: function() {
    console.log("Tu możemy anulować funkcje nasłuchujące");
  },

  render: function() {
    return React.createElement(
      "div",
      { className: "counter" },
      React.createElement("h2", {}, "Counter"),
      React.createElement("span", {}, "Licznik: " + this.state.counter),
      React.createElement(
        "div",
        {},
        React.createElement(
          "button",
          {
            className: "incr-btn",
            onClick: this.increment
          },
          "Increment Counter"
        ),

        React.createElement(
          "button",
          {
            onClick: this.decrement
          },
          "Decrement Counter"
        )
      )
    );
  }
});

const element = React.createElement(
  "div",
  { className: "counters" },
  React.createElement("h1", {}, "Counters"),
  React.createElement(Counter),
  React.createElement(Counter),
  React.createElement(Counter)
);

ReactDOM.render(element, document.getElementById("app"));
